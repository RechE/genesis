package com.example.genesistestproject.base.db

object DbSettings {

    const val DATABASE_VERSION = 1
    const val DATABASE_NAME = "genesis"

}