package com.example.genesistestproject.base.extensions

import android.annotation.SuppressLint
import android.view.View
import androidx.annotation.FloatRange
import androidx.dynamicanimation.animation.DynamicAnimation
import androidx.dynamicanimation.animation.SpringAnimation
import androidx.dynamicanimation.animation.SpringForce

infix fun <T> List<T>.combine(list: List<T>): MutableList<T> {
    val listOf = mutableListOf<T>()
    listOf.addAll(this)
    listOf.addAll(list)
    return listOf
}

@SuppressLint("Range")
fun createSpringAnimation(view: View,
                          property: DynamicAnimation.ViewProperty,
                          finalPosition: Float,
                          @FloatRange(from = 0.0) stiffness: Float,
                          @FloatRange(from = 0.0) dampingRatio: Float): SpringAnimation {
    val animation = SpringAnimation(view, property)
    val spring = SpringForce(finalPosition)
    spring.stiffness = stiffness
    spring.dampingRatio = dampingRatio
    animation.spring = spring
    return animation
}
