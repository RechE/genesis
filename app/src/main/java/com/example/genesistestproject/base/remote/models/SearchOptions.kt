package com.example.genesistestproject.base.remote.models

data class SearchOptions(val query: String)
