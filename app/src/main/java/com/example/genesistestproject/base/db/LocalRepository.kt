package com.example.genesistestproject.base.db

import com.example.genesistestproject.base.remote.models.GithubRepositoryModel
import com.example.genesistestproject.base.remote.models.Result
import com.example.vjetgrouptestapp.base.remote.models.ErrorEntity


class LocalRepository(private val databaseDao: DatabaseDao) {

    fun saveGithubRepo(githubRepositoryModel: GithubRepositoryModel) = try {
        databaseDao.saveGithubRepo(githubRepositoryModel.apply {
            insertedAt = System.currentTimeMillis()
        })
        Result.Success(null)
    } catch (e: Throwable) {
        e.printStackTrace()
        Result.Error(ErrorEntity.DbException(e.localizedMessage))
    }

    fun deleteGithubRepoById(repoId: Int?) = try {
        databaseDao.deleteGithubRepoById(repoId)
        Result.Success(null)
    } catch (e: Throwable) {
        e.printStackTrace()
        Result.Error(ErrorEntity.DbException(e.localizedMessage))
    }

    fun getGithubRepos() = try {
        val pagedList = databaseDao.getGithubRepos()
        Result.Success(pagedList)
    } catch (e: Throwable) {
        e.printStackTrace()
        Result.Error(ErrorEntity.DbException(e.localizedMessage))
    }

    fun getGithubReposIds() = try {
        databaseDao.getGithubReposIds()
    } catch (e: Throwable) {
        e.printStackTrace()
        listOf<Int>()
    }


}