package com.example.genesistestproject.base.db

import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.example.genesistestproject.base.remote.models.GithubRepositoryModel

@Dao
abstract class DatabaseDao {

    @Insert(onConflict = REPLACE)
    abstract fun saveGithubRepo(githubRepositoryModel: GithubRepositoryModel)

    @Query("DELETE FROM repo WHERE id = :repoId")
    abstract fun deleteGithubRepoById(repoId: Int?)

    @Query("SELECT * FROM repo ORDER BY insertedAt DESC")
    abstract fun getGithubRepos(): DataSource.Factory<Int, GithubRepositoryModel>

    @Query("SELECT id FROM repo")
    abstract fun getGithubReposIds(): List<Int>

}