package com.example.genesistestproject.base.extensions

import android.content.Context
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity

fun AppCompatActivity.hideKeyboard() {
    val view = this.currentFocus
    view?.apply {
        val imm = getInputMethodManager()
        imm.hideSoftInputFromWindow(this.windowToken, 0)
    }
}

fun AppCompatActivity.showKeyboard() {
    val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY)
}

fun AppCompatActivity.showKeyboard(editText: EditText) {
    val imm = getInputMethodManager()
    imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT)
}