package com.example.genesistestproject.base.utils

import android.annotation.SuppressLint
import android.util.DisplayMetrics
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.widget.ImageView
import androidx.core.view.marginTop
import androidx.databinding.BindingAdapter
import androidx.dynamicanimation.animation.SpringAnimation
import androidx.dynamicanimation.animation.SpringForce
import com.bumptech.glide.Glide
import com.bumptech.glide.load.MultiTransformation
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions.bitmapTransform
import com.example.genesistestproject.base.extensions.createSpringAnimation
import com.example.genesistestproject.base.extensions.getWindowManager
import com.example.genesistestproject.base.extensions.toPx
import com.example.genesistestproject.base.remote.models.GithubRepositoryModel
import com.example.genesistestproject.ui.search.adapters.GithubSearchPagingAdapterListener


const val TAG = "BindingAdapter"

@BindingAdapter("app:visibility")
fun setVisibility(view: View, isVisible: Boolean) {
    view.visibility = if (isVisible) View.VISIBLE else View.GONE
}

@BindingAdapter("app:invisible")
fun setInvisibility(view: View, isInvisible: Boolean) {
    view.visibility = if (isInvisible) View.INVISIBLE else View.VISIBLE
}

@BindingAdapter(
    value = [
        "app:loadImage",
        "app:cornerRadius"
    ], requireAll = false
)
fun loadImage(imageView: ImageView, url: String?, cornerRadius: Int?) {
    if (url == null) return
    val multiTransformation = MultiTransformation(
        CenterCrop(),
        RoundedCorners(cornerRadius?.toPx() ?: 0)
    )

    Glide.with(imageView.context)
        .load(url)
        .apply(bitmapTransform(multiTransformation))
        .transition(DrawableTransitionOptions.withCrossFade())
        .into(imageView)
}

@SuppressLint("ClickableViewAccessibility")
@BindingAdapter("app:springAnimation", "app:callback")
fun setSpringAnimation(
    imageView: ImageView,
    boolean: Boolean?,
    listener: GithubSearchPagingAdapterListener<GithubRepositoryModel>
) {
    val displayMetrics = DisplayMetrics()
    imageView.context.getWindowManager().defaultDisplay.getMetrics(displayMetrics)
    val width = displayMetrics.widthPixels
    val itemMarginStart = 10.toPx()
    val headHalfWidth = 22.toPx()
    val positionX = width / 2 - headHalfWidth - itemMarginStart
    val positionY = imageView.marginTop
    val xAnimation: SpringAnimation = createSpringAnimation(
        imageView,
        SpringAnimation.X,
        positionX.toFloat(),
        SpringForce.STIFFNESS_LOW,
        SpringForce.DAMPING_RATIO_HIGH_BOUNCY
    )
    val yAnimation: SpringAnimation = createSpringAnimation(
        imageView, SpringAnimation.Y, positionY.toFloat(),
        SpringForce.STIFFNESS_LOW,
        SpringForce.DAMPING_RATIO_HIGH_BOUNCY
    )
    var dX = 0f
    var dY = 0f
    imageView.setOnTouchListener { view, event ->
        when (event.actionMasked) {
            MotionEvent.ACTION_DOWN -> {
                listener.freezeRecycler(true)
                // capture the difference between view's top left corner and touch point
                dX = view.x - event.rawX
                dY = view.y - event.rawY

                // cancel animations so we can grab the view during previous animation
                xAnimation.cancel()
                yAnimation.cancel()
            }
            MotionEvent.ACTION_MOVE -> {
                //  a different approach would be to change the view's LayoutParams.
                imageView.animate()
                    .x(event.rawX + dX)
                    .y(event.rawY + dY)
                    .setDuration(0)
                    .start()
            }
            MotionEvent.ACTION_UP -> {
                listener.freezeRecycler(false)
                xAnimation.start()
                yAnimation.start()
            }
        }
        true
    }
}