package com.example.genesistestproject.base.remote

import com.example.genesistestproject.base.arch.GeneralErrorHandle
import com.example.genesistestproject.base.extensions.combine
import com.example.genesistestproject.base.remote.models.GithubRepositoryModel
import com.example.genesistestproject.base.remote.models.Result
import retrofit2.Retrofit

class RemoteRepository(
    retrofit: Retrofit,
    private val generalErrorHandle: GeneralErrorHandle
) {

    private val api: Api = retrofit.create(Api::class.java)

    suspend fun getGithubRepositoriesInParallel(
        pageSize: Int,
        page: Int,
        query: String
    ): Result<List<GithubRepositoryModel>> = try {
        val githubRepsListResponsePartOne = api.getRepositoriesAsync(
            page = page * 2 - 1,
            perPage = pageSize / 2,
            query = query
        )
        val githubRepsListResponsePartTwo = api.getRepositoriesAsync(
            page = page * 2,
            perPage = pageSize / 2,
            query = query
        )
        val combinedResult =
            githubRepsListResponsePartOne.await().items combine githubRepsListResponsePartTwo.await().items
        Result.Success(combinedResult)
    } catch (throwable: Throwable) {
        Result.Error(generalErrorHandle.getError(throwable))
    }
}