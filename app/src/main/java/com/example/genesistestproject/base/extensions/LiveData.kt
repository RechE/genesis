package com.example.genesistestproject.base.extensions

import android.os.Handler
import android.os.Looper
import androidx.lifecycle.*

operator fun <T> MutableLiveData<ArrayList<T>>.plusAssign(values: List<T>) {
    val value = this.value ?: arrayListOf()
    value.addAll(values)
    this.value = value
}

fun <T> MutableLiveData<ArrayList<T>>.addItemAtPosition(pos: Int, item: T) {
    val value = this.value ?: arrayListOf()
    value.add(pos, item)
    this.value = value
}

fun <T> MutableLiveData<ArrayList<T>>.addItem(item: T) {
    val value = this.value ?: arrayListOf()
    value.add(item)
    this.value = value
}

fun <T> MutableLiveData<ArrayList<T>>.removeItem(item: T) {
    val value = this.value ?: arrayListOf()
    value.remove(item)
    this.value = value
}

fun <T> LiveData<T>.reObserve(owner: LifecycleOwner, observer: Observer<T>) {
    removeObserver(observer)
    observe(owner, observer)
}

fun <T> LiveData<T>.debounce(mills: Long): MutableLiveData<T> {
    return MediatorLiveData<T>().also { mld ->
        val source = this
        val handler = Handler(Looper.getMainLooper())
        val runnable = Runnable {
            mld.value = source.value
        }

        mld.addSource(source) {
            println("Accept : $it")
            handler.removeCallbacks(runnable)
            handler.postDelayed(runnable, mills)
        }
    }
}

fun <T> LiveData<T>.filter(predicate: (T) -> Boolean): MutableLiveData<T> {
    return MediatorLiveData<T>().also { mld ->
        val source = this
        mld.addSource(source) {
            if (predicate.invoke(it))
                mld.value = source.value
        }
    }
}
