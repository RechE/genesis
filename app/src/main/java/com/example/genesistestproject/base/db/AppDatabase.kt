package com.example.genesistestproject.base.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.genesistestproject.base.remote.models.GithubRepositoryModel
import com.example.genesistestproject.base.remote.models.Owner

@Database(
    entities = [
        GithubRepositoryModel::class
    ],
    version = DbSettings.DATABASE_VERSION,
    exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun databaseRepository(): DatabaseDao
}