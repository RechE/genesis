package com.example.genesistestproject.base.arch

import android.util.Log
import androidx.lifecycle.ViewModel
import com.example.genesistestproject.R
import com.example.genesistestproject.base.extensions.getRes
import com.example.genesistestproject.base.remote.models.Result
import com.example.genesistestproject.base.utils.SingleLiveEvent
import com.example.vjetgrouptestapp.base.remote.models.ErrorEntity

const val ERROR_TAG = "error"

abstract class BaseViewModel : ViewModel() {

    val progressVisibility =
        SingleLiveEvent<Boolean>()

    protected fun showLoading(showLoading: Boolean) {
        if (showLoading) {
            showLoading()
        } else {
            hideLoading()
        }
    }

    protected fun showLoading() {
        progressVisibility.value = true
    }

    protected fun hideLoading() {
        progressVisibility.value = false
    }

    private fun showError(
        message: String?,
        cancelListener: (() -> Unit)? = null,
        retryListener: (() -> Unit)? = null
    ) {

    }

    fun handleErrors(
        result: Result.Error
    ) {
        Log.e(ERROR_TAG, "handleErrors: ${result.error.message}")
        when (result.error) {
            is ErrorEntity.NoNetwork -> {
                showError(R.string.error_no_network.getRes())
            }
            is ErrorEntity.Unauthorized -> {

            }
            is ErrorEntity.BadRequest -> {

            }
            else -> {
                showError("something went wrong")
            }
        }
    }
}
