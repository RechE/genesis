package com.example.genesistestproject.base.remote.interceptors

import com.example.genesistestproject.R
import com.example.genesistestproject.base.App
import java.io.IOException

class NoNetworkException : IOException(App.applicationContext().getString(R.string.error_no_network))