package com.example.genesistestproject.base.remote

object RemoteSettings {

    const val BASE_HTTP_URL = "https://api.github.com/"
    const val READ_TIMEOUT = 50L
    const val CONNECT_TIMEOUT = 60L
    const val WRITE_TIMEOUT = 120L
}