package com.example.genesistestproject.base.remote

import com.example.genesistestproject.base.remote.models.GithubRepsListResponse
import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Query

interface Api {
    @GET("search/repositories")
    fun getRepositoriesAsync(
        @Query("page") page: Int,
        @Query("q") query: String,
        @Query("per_page") perPage: Int
    ): Deferred<GithubRepsListResponse>
}