package com.example.genesistestproject.ui

import com.example.genesistestproject.R
import com.example.genesistestproject.base.arch.BaseActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeActivity : BaseActivity<HomeActivityViewModel>() {

    override fun viewModelClass(): Class<HomeActivityViewModel> =
        HomeActivityViewModel::class.java


    override fun layoutResId(): Int =
        R.layout.activity_home

}