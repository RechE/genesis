package com.example.genesistestproject.ui

import androidx.hilt.lifecycle.ViewModelInject
import com.example.genesistestproject.base.arch.BaseViewModel

class HomeActivityViewModel @ViewModelInject constructor() : BaseViewModel()