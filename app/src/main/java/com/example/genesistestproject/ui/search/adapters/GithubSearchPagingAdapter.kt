package com.example.genesistestproject.ui.search.adapters

import androidx.recyclerview.widget.DiffUtil
import com.example.genesistestproject.R
import com.example.genesistestproject.base.arch.BasePagedListAdapter
import com.example.genesistestproject.base.remote.models.GithubRepositoryModel

class GithubSearchPagingAdapter(listener: GithubSearchPagingAdapterListener<GithubRepositoryModel>) :
    BasePagedListAdapter<GithubSearchItemState, GithubRepositoryModel>(DIFF_CALLBACK, listener) {

    override fun itemState(
        item: GithubRepositoryModel,
        listener: Listener<GithubRepositoryModel>,
        position: Int
    ): GithubSearchItemState =
        GithubSearchItemState(
            item = item,
            position = position,
            listener = listener as GithubSearchPagingAdapterListener<GithubRepositoryModel>
        )

    override fun layoutResId(viewType: Int): Int = R.layout.item_github_search


    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<GithubRepositoryModel>() {
            override fun areItemsTheSame(
                oldItem: GithubRepositoryModel,
                newItem: GithubRepositoryModel
            ): Boolean =
                oldItem.name == newItem.name

            override fun areContentsTheSame(
                oldItem: GithubRepositoryModel,
                newItem: GithubRepositoryModel
            ): Boolean =
                oldItem == newItem
        }
    }

}

interface GithubSearchPagingAdapterListener<I> : BasePagedListAdapter.Listener<I> {
    fun onFavClick(model: GithubRepositoryModel) {}
    fun freezeRecycler(freeze: Boolean) {}
}

