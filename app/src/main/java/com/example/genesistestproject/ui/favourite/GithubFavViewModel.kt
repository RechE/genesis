package com.example.genesistestproject.ui.favourite

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.example.genesistestproject.base.arch.BaseViewModel
import com.example.genesistestproject.base.db.LocalRepository
import com.example.genesistestproject.base.remote.models.GithubRepositoryModel
import com.example.genesistestproject.base.remote.models.Result

private const val DEFAULT_PAGE_SIZE = 30
private const val PREFETCH_DISTANCE = DEFAULT_PAGE_SIZE / 2
private const val INITIAL_LOAD_SIZE = DEFAULT_PAGE_SIZE


class GithubFavViewModel @ViewModelInject constructor(private val localRepository: LocalRepository) :
    BaseViewModel() {

    lateinit var pagedList: LiveData<PagedList<GithubRepositoryModel>>

    init {
        when (val result = localRepository.getGithubRepos()) {
            is Result.Success -> {
                pagedList = getFavReposPagedList(result.data)
            }
            is Result.Error -> handleErrors(result)
        }
    }

    fun onFavClick(model: GithubRepositoryModel) {
        when (val result = localRepository.deleteGithubRepoById(model.id)) {
            is Result.Success -> {

            }
            is Result.Error -> handleErrors(result)
        }
    }

    private fun getFavReposPagedList(
        dataSource: DataSource.Factory<Int, GithubRepositoryModel>
    ): LiveData<PagedList<GithubRepositoryModel>> {
        return LivePagedListBuilder(
            dataSource,
            getPageConfig()
        ).build()
    }

    private fun getPageConfig(): PagedList.Config =
        PagedList.Config.Builder()
            .setPageSize(DEFAULT_PAGE_SIZE)
            .setInitialLoadSizeHint(INITIAL_LOAD_SIZE)
            .setPrefetchDistance(PREFETCH_DISTANCE)
            .setEnablePlaceholders(false)
            .build()

}
