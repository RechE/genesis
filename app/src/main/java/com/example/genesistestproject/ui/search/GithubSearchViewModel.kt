package com.example.genesistestproject.ui.search

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.viewModelScope
import androidx.paging.DataSource
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.example.genesistestproject.R
import com.example.genesistestproject.base.arch.BaseViewModel
import com.example.genesistestproject.base.db.LocalRepository
import com.example.genesistestproject.base.extensions.falseIfNull
import com.example.genesistestproject.base.extensions.getRes
import com.example.genesistestproject.base.remote.RemoteRepository
import com.example.genesistestproject.base.remote.models.GithubRepositoryModel
import com.example.genesistestproject.base.remote.models.Result
import com.example.genesistestproject.base.remote.models.SearchOptions
import com.example.genesistestproject.ui.search.paging.GithubRemoteDataSource
import com.example.genesistestproject.ui.search.paging.RemoteFetchState


private const val DEFAULT_PAGE_SIZE = 30
private const val PREFETCH_DISTANCE = DEFAULT_PAGE_SIZE / 2
private const val INITIAL_LOAD_SIZE = DEFAULT_PAGE_SIZE

class GithubSearchViewModel @ViewModelInject constructor(
    val remoteRepository: RemoteRepository,
    val localRepository: LocalRepository
) : BaseViewModel() {

    private val searchOptions = MutableLiveData<SearchOptions>()
    val searchQuery = MutableLiveData<String>()
    val showEmptyContainer = MutableLiveData<Boolean>()

    val showSearchBar = MutableLiveData<Boolean>().apply { value = false }
    val showDoneBtn: LiveData<Boolean> = Transformations.map(searchQuery) {
        isSearchQueryValid(it)
    }
    val pagedList: LiveData<PagedList<GithubRepositoryModel>> =
        Transformations.switchMap(searchOptions) {
            getFeedPagingList(it)
        }

    fun setDefaultSearchOptions() {
        if (searchOptions.value != null) return
        searchQuery.value = R.string.android.getRes()
        searchOptions.value = SearchOptions(query = R.string.android.getRes())
    }

    fun applySearch() {
        if (!isSearchQueryValid(searchQuery.value)) return
        searchOptions.value = SearchOptions(query = searchQuery.value!!)
    }

    private fun isSearchQueryValid(query: String?): Boolean {
        return query != null && query.length > 2
    }

    fun onFavClick(model: GithubRepositoryModel) {
        val result =
            if (model.isFavourite.falseIfNull())
                localRepository.saveGithubRepo(model)
            else localRepository.deleteGithubRepoById(model.id)
        when (result) {
            is Result.Success -> {

            }
            is Result.Error -> handleErrors(result)
        }
    }

    fun handleRemoteState(remoteFetchState: RemoteFetchState) {
        when (remoteFetchState) {
            is RemoteFetchState.Loading -> {
                showEmptyContainer.value = false
                showLoading(remoteFetchState.showLoading)
            }
            is RemoteFetchState.Empty -> showEmptyContainer.value = true
        }
    }

    private fun getFeedPagingList(
        searchOptions: SearchOptions
    ): LiveData<PagedList<GithubRepositoryModel>> {
        return LivePagedListBuilder(
            getRemoteSourceFactory(searchOptions),
            getPageConfig()
        ).build()
    }

    private fun getRemoteSourceFactory(searchOptions: SearchOptions) =
        object : DataSource.Factory<Int, GithubRepositoryModel>() {
            override fun create(): DataSource<Int, GithubRepositoryModel> {
                return GithubRemoteDataSource(
                    searchOptions = searchOptions,
                    remoteRepository = remoteRepository,
                    localRepository = localRepository,
                    coroutineScope = viewModelScope,
                    stateCallback = { handleRemoteState(it) },
                    errorCallback = { handleErrors(it) }
                )
            }
        }

    private fun getPageConfig(): PagedList.Config =
        PagedList.Config.Builder()
            .setPageSize(DEFAULT_PAGE_SIZE)
            .setInitialLoadSizeHint(INITIAL_LOAD_SIZE)
            .setPrefetchDistance(PREFETCH_DISTANCE)
            .setEnablePlaceholders(false)
            .build()

}