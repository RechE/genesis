package com.example.genesistestproject.ui.search.paging

sealed class RemoteFetchState {
    class Loading(val showLoading: Boolean) : RemoteFetchState()
    class Error(message: String) : RemoteFetchState()
    object Empty : RemoteFetchState()
}