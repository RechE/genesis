package com.example.genesistestproject.ui.search.paging

import android.util.Log
import androidx.paging.PageKeyedDataSource
import com.example.genesistestproject.base.db.LocalRepository
import com.example.genesistestproject.base.remote.RemoteRepository
import com.example.genesistestproject.base.remote.models.GithubRepositoryModel
import com.example.genesistestproject.base.remote.models.Result
import com.example.genesistestproject.base.remote.models.SearchOptions
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

const val INITIAL_PAGE_KEY = 1
const val TAG = "GithubRemoteDataSource"

class GithubRemoteDataSource(
    val searchOptions: SearchOptions,
    val remoteRepository: RemoteRepository,
    val localRepository: LocalRepository,
    val coroutineScope: CoroutineScope,
    val errorCallback: (Result.Error) -> Unit,
    val stateCallback: (RemoteFetchState) -> Unit
) :
    PageKeyedDataSource<Int, GithubRepositoryModel>() {
    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, GithubRepositoryModel>
    ) {
        Log.d(
            TAG,
            "loadInitial: perPage ${params.requestedLoadSize}"
        )
        coroutineScope.launch {
            stateCallback.invoke(
                RemoteFetchState.Loading(
                    true
                )
            )
            val result = remoteRepository.getGithubRepositoriesInParallel(
                page = INITIAL_PAGE_KEY,
                pageSize = params.requestedLoadSize,
                query = searchOptions.query
            )
            stateCallback.invoke(
                RemoteFetchState.Loading(
                    false
                )
            )
            when (result) {
                is Result.Success -> {
                    val listOfFavDBIds = localRepository.getGithubReposIds()
                    if (result.data.isNullOrEmpty()) stateCallback.invoke(RemoteFetchState.Empty)
                    attachDBFavouriteStatus(result.data, listOfFavDBIds)
                    callback.onResult(
                        result.data, null,
                        INITIAL_PAGE_KEY.plus(1)
                    )
                }
                is Result.Error -> errorCallback.invoke(result)
            }
        }
    }

    override fun loadAfter(
        params: LoadParams<Int>,
        callback: LoadCallback<Int, GithubRepositoryModel>
    ) {
        Log.d(
            TAG,
            "loadAfter: perPage ${params.requestedLoadSize} page ${params.key}"
        )
        coroutineScope.launch {
            val result = remoteRepository.getGithubRepositoriesInParallel(
                page = params.key,
                pageSize = params.requestedLoadSize,
                query = searchOptions.query
            )
            when (result) {
                is Result.Success -> {
                    val listOfFavDBIds = localRepository.getGithubReposIds()
                    attachDBFavouriteStatus(result.data, listOfFavDBIds)
                    callback.onResult(
                        result.data, params.key.plus(1)
                    )
                }
                is Result.Error -> errorCallback.invoke(result)
            }
        }
    }

    override fun loadBefore(
        params: LoadParams<Int>,
        callback: LoadCallback<Int, GithubRepositoryModel>
    ) {

    }

    private fun attachDBFavouriteStatus(
        list: List<GithubRepositoryModel>,
        listOfFavDBIds: List<Int>
    ) {
        list.forEach { remoteModel ->
            if (listOfFavDBIds.any { remoteModel.id == it }) {
                remoteModel.isFavourite = true
            }
        }
    }

}