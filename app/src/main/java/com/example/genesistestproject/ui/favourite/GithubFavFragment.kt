package com.example.genesistestproject.ui.favourite

import android.os.Bundle
import android.view.View
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.example.genesistestproject.R
import com.example.genesistestproject.base.arch.BaseFragment
import com.example.genesistestproject.base.remote.models.GithubRepositoryModel
import com.example.genesistestproject.ui.search.adapters.GithubSearchPagingAdapter
import com.example.genesistestproject.ui.search.adapters.GithubSearchPagingAdapterListener
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_github_fav.*
import kotlinx.android.synthetic.main.fragment_github_search.rv_search


@AndroidEntryPoint
class GithubFavFragment : BaseFragment<GithubFavViewModel>() {

    private val sharedViewModel: GithubSharedViewModel by activityViewModels()
    private var searchAdapter: GithubSearchPagingAdapter? = null

    override fun viewModelClass(): Class<GithubFavViewModel> =
        GithubFavViewModel::class.java

    override fun layoutResId(): Int =
        R.layout.fragment_github_fav

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initSearchAdapter()
        setupBackBtn()
    }

    override fun subscribeToEvents() {
        viewModel.pagedList.observe(this, Observer {
            searchAdapter?.submitList(it)
        })
    }

    private fun setupBackBtn() {
        btn_back.setOnClickListener {
            findNavController().popBackStack()
        }
    }

    private fun initSearchAdapter() {
        searchAdapter = GithubSearchPagingAdapter(
            object : GithubSearchPagingAdapterListener<GithubRepositoryModel> {
                override fun onFavClick(model: GithubRepositoryModel) {
                    sharedViewModel.collect(model.id)
                    viewModel.onFavClick(model)
                }

                override fun freezeRecycler(freeze: Boolean) {
                    rv_search.isLayoutFrozen = freeze
                }
            }
        )
        rv_search.adapter = searchAdapter
    }
}