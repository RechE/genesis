package com.example.genesistestproject.ui.search.adapters

import androidx.lifecycle.MutableLiveData
import com.example.genesistestproject.base.arch.BaseItemState
import com.example.genesistestproject.base.extensions.falseIfNull
import com.example.genesistestproject.base.remote.models.GithubRepositoryModel

class GithubSearchItemState(
    val item: GithubRepositoryModel,
    val position: Int,
    val listener: GithubSearchPagingAdapterListener<GithubRepositoryModel>
) : BaseItemState() {

    private val title = MutableLiveData<String>()
    val image = MutableLiveData<String>()
    val isFavourite = MutableLiveData<Boolean>()

    init {
        title.value = item.description
        image.value = item.owner?.avatar_url
        isFavourite.value = item.isFavourite.falseIfNull()
    }

    fun onFavoriteClick() {
        item.isFavourite = !item.isFavourite.falseIfNull()
        isFavourite.value = item.isFavourite
        listener.onFavClick(item)
    }
}