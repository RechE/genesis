package com.example.genesistestproject.ui.search

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.os.Bundle
import android.view.View
import android.view.ViewAnimationUtils
import android.view.inputmethod.EditorInfo
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.paging.PagedList
import com.example.genesistestproject.R
import com.example.genesistestproject.base.arch.BaseFragment
import com.example.genesistestproject.base.extensions.falseIfNull
import com.example.genesistestproject.base.extensions.hideKeyboard
import com.example.genesistestproject.base.remote.models.GithubRepositoryModel
import com.example.genesistestproject.base.utils.Utils
import com.example.genesistestproject.ui.favourite.GithubSharedViewModel
import com.example.genesistestproject.ui.search.adapters.GithubSearchPagingAdapter
import com.example.genesistestproject.ui.search.adapters.GithubSearchPagingAdapterListener
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_github_search.*
import kotlin.math.hypot


@AndroidEntryPoint
class GithubSearchFragment : BaseFragment<GithubSearchViewModel>() {

    private val sharedViewModel: GithubSharedViewModel by activityViewModels()
    private var searchAdapter: GithubSearchPagingAdapter? = null

    override fun viewModelClass(): Class<GithubSearchViewModel> =
        GithubSearchViewModel::class.java

    override fun layoutResId(): Int =
        R.layout.fragment_github_search

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initSearchAdapter()
        setupSearch()
        setupFav()
        viewModel.setDefaultSearchOptions()
    }

    override fun subscribeToEvents() {
        super.subscribeToEvents()
        viewModel.pagedList.observe(this, Observer {
            attachFavState(it)
            searchAdapter?.submitList(it)
        })
    }

    private fun initSearchAdapter() {
        searchAdapter = GithubSearchPagingAdapter(
            object : GithubSearchPagingAdapterListener<GithubRepositoryModel> {
                override fun onFavClick(model: GithubRepositoryModel) {
                    viewModel.onFavClick(model)
                }

                override fun freezeRecycler(freeze: Boolean) {
                    rv_search.isLayoutFrozen = freeze
                }
            }
        )
        rv_search.adapter = searchAdapter
    }

    private fun setupSearch() {
        btn_search.setOnClickListener {
            if (!viewModel.showSearchBar.value.falseIfNull()) {
                revealSearchBar(search_bar)
            } else {
                hideSearchBar(search_bar)
            }
        }
        et_search.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                viewModel.applySearch()
                (activity as AppCompatActivity).hideKeyboard()
                true
            } else false
        }
        btn_done.setOnClickListener {
            viewModel.applySearch()
            (activity as AppCompatActivity).hideKeyboard()
        }
    }

    private fun setupFav() {
        btn_fav.setOnClickListener {
            val action =
                GithubSearchFragmentDirections.actionGithubSearchFragmentToGithubFavFragment()
            findNavController().navigate(action)
        }
    }

    private fun attachFavState(pagedList: PagedList<GithubRepositoryModel>) {
        val listOfRemoveIDs = sharedViewModel.getWithReset()
        if (listOfRemoveIDs.size == 0 && pagedList.size == 0) return
        for (i in 0 until pagedList.size) {
            val repo = pagedList.getOrNull(i)
            if (listOfRemoveIDs.any { it == repo?.id }) {
                repo?.isFavourite = false
            }
        }
    }

    private fun revealSearchBar(view: View) {
        val cx = view.width / 2
        val cy = view.height / 2
        val finalRadius =
            hypot(cx.toDouble(), cy.toDouble()).toFloat()
        val anim: Animator = ViewAnimationUtils.createCircularReveal(view, cx, cy, 0f, finalRadius)
        viewModel.showSearchBar.value = true
        anim.start()
    }

    private fun hideSearchBar(view: View) {
        val cx = view.width / 2
        val cy = view.height / 2
        val initialRadius =
            hypot(cx.toDouble(), cy.toDouble()).toFloat()
        val anim: Animator =
            ViewAnimationUtils.createCircularReveal(view, cx, cy, initialRadius, 0f)
        anim.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator?) {
                super.onAnimationEnd(animation)
                viewModel.showSearchBar.value = false
            }
        })
        anim.start()
    }

}