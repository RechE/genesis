package com.example.genesistestproject.ui.favourite

import com.example.genesistestproject.base.arch.BaseViewModel

class GithubSharedViewModel : BaseViewModel() {
    var listOfRemovedFavIds = mutableListOf<Int?>()

    fun collect(id: Int?) {
        listOfRemovedFavIds.add(id)
    }

    fun getWithReset(): MutableList<Int?> {
        val temp = listOfRemovedFavIds
        listOfRemovedFavIds = mutableListOf()
        return temp
    }
}